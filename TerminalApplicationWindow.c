#include <gtk/gtk.h>

#include "TerminalApplication.h"
#include "TerminalApplicationWindow.h"
#include "TerminalTextView.h"

#define MASTER_BUF_SIZE 512

struct _TerminalAppWindow
{
  GtkApplicationWindow parent;
};

typedef struct _TerminalAppWindowPrivate TerminalAppWindowPrivate;

struct _TerminalAppWindowPrivate
{
  gint master_fd;
  char master_buf[MASTER_BUF_SIZE];

  GtkWidget *text_view;
  GtkWidget *view_scroller;
};

G_DEFINE_TYPE_WITH_PRIVATE (TerminalAppWindow, terminal_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
terminal_app_window_init (TerminalAppWindow *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
}

static void
terminal_app_window_class_init (TerminalAppWindowClass *class)
{
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class),
                                               "/org/gtk/katerok/terminal/MainWindow.ui");
  gtk_widget_class_bind_template_child_private (GTK_WIDGET_CLASS (class), TerminalAppWindow, view_scroller);
}

TerminalAppWindow *
terminal_app_window_new (TerminalApp *app,
                         gint master_fd)
{
  TerminalAppWindow *new_object;
  TerminalAppWindowPrivate *priv;

  new_object = g_object_new (TERMINAL_APP_WINDOW_TYPE, "application", app, NULL);

  priv = terminal_app_window_get_instance_private (new_object);
  priv->master_fd = master_fd;
  priv->text_view = GTK_WIDGET (terminal_text_view_new());

  gtk_container_add(GTK_CONTAINER(priv->view_scroller), priv->text_view);
  gtk_widget_show(priv->text_view);

  return new_object;
}

static gboolean
terminal_app_window_on_key_press (GtkWidget *widget,
                                  GdkEventKey *event,
                                  gpointer data)
{
  TerminalAppWindowPrivate *priv = data;
  write(priv->master_fd, &(event->keyval), 1);

  return FALSE;
}

static gboolean
terminal_app_window_on_idle(gpointer data)
{
  TerminalAppWindowPrivate *priv = data;
  struct timeval timeout = {0, 5};
  fd_set in_fds;
  ssize_t num_read;

  FD_ZERO (&in_fds);
  FD_SET (priv->master_fd, &in_fds);

  if (select (priv->master_fd + 1, &in_fds, NULL, NULL, &timeout) == -1)
    return 1;

  if (!FD_ISSET (priv->master_fd, &in_fds))
    return 1;

  num_read = read (priv->master_fd, priv->master_buf, MASTER_BUF_SIZE);


  if (num_read > 0)
    {
      terminal_text_view_insert (TERMINAL_TEXT_VIEW (priv->text_view),
                                 priv->master_buf, num_read);
    }

  return 1;
}

void
terminal_app_window_open (TerminalAppWindow *win)
{
  TerminalAppWindowPrivate *priv;

  priv = terminal_app_window_get_instance_private (win);
  g_signal_connect (G_OBJECT (priv->text_view), "key_press_event",
                    G_CALLBACK (terminal_app_window_on_key_press), priv);
  g_idle_add (terminal_app_window_on_idle, priv);
}

#undef MASTER_BUF_SIZE
