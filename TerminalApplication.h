#ifndef __TERMINALAPP_H
#define __TERMINALAPP_H

#include <gtk/gtk.h>


#define TERMINAL_APP_TYPE (terminal_app_get_type ())
G_DECLARE_FINAL_TYPE (TerminalApp, terminal_app, TERMINAL, APP, GtkApplication)


TerminalApp     *terminal_app_new         (void);


#endif /* __TERMINALAPP_H */
