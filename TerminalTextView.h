#ifndef __TERMINALTEXTVIEW_H
#define __TERMINALTEXTVIEW_H

#include <gtk/gtk.h>

#define TERMINAL_TEXT_VIEW_TYPE (terminal_text_view_get_type ())
G_DECLARE_FINAL_TYPE (TerminalTextView, terminal_text_view, TERMINAL, TEXT_VIEW, GtkTextView)


TerminalTextView     *terminal_text_view_new            (void);
void                  terminal_text_view_insert         (TerminalTextView *text_view,
                                                         const char *string,
                                                         ssize_t lenght);


#endif // __TERMINALTEXTVIEW_H

