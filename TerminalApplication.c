#include <gtk/gtk.h>

#include "TerminalApplication.h"
#include "TerminalApplicationWindow.h"
#include "PtyUtils.h"

#define MAX_SLAVE_NAME 1000

struct _TerminalApp
{
  GtkApplication parent;
};

typedef struct _TerminalAppPrivate TerminalAppPrivate;

struct _TerminalAppPrivate
{
  gint master_fd;
  struct termios pty_original_properties;
};

G_DEFINE_TYPE_WITH_PRIVATE (TerminalApp, terminal_app, GTK_TYPE_APPLICATION);

static void
terminal_app_init (TerminalApp *app)
{
  char slave_name[MAX_SLAVE_NAME];
  int master_fd;
  struct winsize window_size;
  pid_t child_pid;
  TerminalAppPrivate *priv;

  priv = terminal_app_get_instance_private (TERMINAL_APP (app));

  // Get attributes and window size of current terminal to pass them to
  // the pseudoterminal slave
  if (tcgetattr (STDIN_FILENO, &(priv->pty_original_properties)) == -1)
    pty_utils_error_exit ("tcgetattr");
  if (ioctl (STDIN_FILENO, TIOCGWINSZ, &window_size) < 0)
    pty_utils_error_exit ("ioctl-TIOCGWINSZ");

  child_pid = pty_utils_fork (&master_fd, slave_name, MAX_SLAVE_NAME,
                              &(priv->pty_original_properties), &window_size);

  // ERROR
  if (child_pid == -1)
    pty_utils_error_exit ("ptyFork");

  // CHILD: execute a shell on pty slave
  if (child_pid == 0)
    {
      char *shell;

      shell = getenv ("SHELL");
      if (shell == NULL || *shell == '\0')
        shell = "/bin/sh";
      execlp (shell, shell, (char *) NULL);
      // ERROR: we should not get here
      pty_utils_error_exit ("execlp");
    }

  // PARENT: relay data between terminal and pty master
  priv->master_fd = master_fd;
}

static void
terminal_app_activate (GApplication *app)
{
  TerminalAppPrivate *priv;
  TerminalAppWindow *win;

  priv = terminal_app_get_instance_private (TERMINAL_APP(app));
  win = terminal_app_window_new (TERMINAL_APP (app), priv->master_fd);
  terminal_app_window_open (win);

  gtk_window_present (GTK_WINDOW (win));
}

static void
terminal_app_on_shutdown (gpointer app)
{
  TerminalAppPrivate *priv;
  priv = terminal_app_get_instance_private (TERMINAL_APP (app));

  tcsetattr (STDIN_FILENO, TCSANOW, &(priv->pty_original_properties));
  exit (EXIT_SUCCESS);
}

TerminalApp *
terminal_app_new (void)
{
  TerminalApp *new_object;

  new_object = g_object_new (TERMINAL_APP_TYPE,
                             "application-id", "org.gtk.katya.terminal",
                             "flags", G_APPLICATION_HANDLES_OPEN,
                             NULL);

  g_signal_connect (G_OBJECT (new_object),
                    "shutdown", G_CALLBACK (terminal_app_on_shutdown),
                    new_object);

  return new_object;
}

static void
terminal_app_class_init (TerminalAppClass *class)
{
  G_APPLICATION_CLASS (class)->activate = terminal_app_activate;
}

#undef MAX_SLAVE_NAME

