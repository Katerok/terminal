#include "PtyUtils.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int
pty_utils_properties_set_raw (int fd,
                              struct termios *prev_termios)
{
  struct termios t;

  // Get attributes for modifying
  // and keep the copy for restoring attributes on exit
  if (tcgetattr (fd, &t) == -1)
    return -1;
  if (prev_termios != NULL)
    *prev_termios = t;

  // Setup flags controlling the user interface for terminal input:
  // noncanonical mode, disable signals, extended
  // input processing, and echoing
  t.c_lflag &= ~(ICANON | ISIG | IEXTEN | ECHO);
  // Setup flags controlling terminal input:
  // disable special handling of CR, NL, and BREAK;
  // no 8th-bit stripping or parity error handling;
  // disable START/STOP output flow control
  t.c_iflag &= ~(BRKINT | ICRNL | IGNBRK | IGNCR | INLCR |
                 INPCK | ISTRIP | IXON | PARMRK);
  // Setup  flags controlling terminal output:
  // disable all output processing
  t.c_oflag &= ~OPOST;

  // Setup terminal special characters and fields controlling the
  // operation of noncanonical mode input:
  // 1 character-at-a-time input
  t.c_cc[VMIN] = 1;
  // with blocking
  t.c_cc[VTIME] = 0;

  // Setup new attributes.
  // TCSAFLUSH specifies that we want to wait for output queue to execute
  // and discart all input
  if (tcsetattr (fd, TCSAFLUSH, &t) == -1)
    return -1;

  return 0;
}

void
pty_utils_error_exit (const char *error_string)
{
  printf ("Failed at: %s", error_string);
  _exit (EXIT_FAILURE);
}

int
pty_utils_master_open (char *slave_name,
                       size_t name_len)
{
  int master_fd;

  // Find and open an unused pseudoterminal master device,
  // keep its file descriptor
  master_fd = posix_openpt (O_RDWR | O_NOCTTY);
  if (master_fd == -1)
    return -1;

  // Grant access to slave pty. (optional for Linux)
  // The grantpt() function changes the mode and owner of the
  // slave pseudoterminal device corresponding to the master pseudoterminal
  // referred to by master_fd.
  // On Linux, a pseudoterminal slave is automatically configured.
  if (grantpt (master_fd) == -1)
    {
      close (master_fd);
      return -1;
    }

  // Remove an internal lock on the slave. The purpose of this locking
  // mechanism is to allow the calling process to perform initializations
  // required for the pseudoterminal slave (e.g., calling grantpt())
  // before another process is allowed to open it.
  if (unlockpt (master_fd) == -1)
    {
      close (master_fd);
      return -1;
    }

  // Obtaining the name of the slave
  if (ptsname_r (master_fd, slave_name, name_len) != 0)
    {
      close (master_fd);
      return -1;
    }

  return master_fd;
}

pid_t
pty_utils_fork (int *master_fd,
                char *slave_name,
                size_t slave_name_len,
                const struct termios *slave_termios,
                const struct winsize *slave_ws)
{
  pid_t child_pid;
  *master_fd = pty_utils_master_open (slave_name, slave_name_len);

  if (*master_fd == -1)
    return -1;

  child_pid = fork ();

  // ERROR
  if (child_pid == -1)
    {
      close (*master_fd);
      return -1;
    }

  // PARENT: return master file descriptor and child pid
  if (child_pid != 0)
    return child_pid;

  // CHILD: create a new session and setup preudoterminal slave
  {
    int slave_fd;

    // Start the new session. The child is the leader of the new session
    // and loses its controlling terminal (if it had one)
    if (setsid () == -1)
      pty_utils_error_exit ("setsid");

    // Close master file descriptor. It is not needed for child process
    close (*master_fd);

    // Open the pseudoterminal slave. The pseudoterminal slave becomes
    // the controlling terminal for the child.
    slave_fd = open (slave_name, O_RDWR);
    if (slave_fd == -1)
      pty_utils_error_exit ("open-slave");

    // Setup terminal attributes and window size
    if (tcsetattr (slave_fd, TCSANOW, slave_termios) == -1)
      pty_utils_error_exit ("tcsetattr");
    if (ioctl (slave_fd, TIOCSWINSZ, slave_ws) == -1)
      pty_utils_error_exit ("ioctl-TIOCSWINSZ");

    // Duplicate the slave file descriptor to be the standard input,
    // output, and error for the child.
    // At this point, the child can use the standard file descriptors
    if (dup2 (slave_fd, STDIN_FILENO) != STDIN_FILENO)
      pty_utils_error_exit ("dup2-STDIN_FILENO");
    if (dup2 (slave_fd, STDOUT_FILENO) != STDOUT_FILENO)
      pty_utils_error_exit ("dup2-STDOUT_FILENO");
    if (dup2 (slave_fd, STDERR_FILENO) != STDERR_FILENO)
      pty_utils_error_exit ("dup2-STDERR_FILENO");

    // Safety check I dont get :(
    if (slave_fd > STDERR_FILENO)
      close (slave_fd);

    return 0;
  }

  return -1;
}

