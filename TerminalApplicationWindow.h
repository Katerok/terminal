#ifndef __TERMINALAPPWIN_H
#define __TERMINALAPPWIN_H

#include <gtk/gtk.h>
#include "TerminalApplication.h"


#define TERMINAL_APP_WINDOW_TYPE (terminal_app_window_get_type ())
G_DECLARE_FINAL_TYPE (TerminalAppWindow, terminal_app_window, TERMINAL, APP_WINDOW, GtkApplicationWindow)


TerminalAppWindow       *terminal_app_window_new          (TerminalApp *app,
                                                         gint master_fd);
void                    terminal_app_window_open         (TerminalAppWindow *win);


#endif /* __TERMINALAPPWIN_H */
