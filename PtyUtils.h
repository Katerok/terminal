#ifndef __PTYUTILS_H
#define __PTYUTILS_H

#define _XOPEN_SOURCE 600
#define _GNU_SOURCE

#include <sys/ioctl.h>
#include <termios.h>
#include <stdlib.h>

// Print error message and exit
void     pty_utils_error_exit            (const char *error_string);
// Open a pseudoterminal master and obtain the name of
// the corresponding pseudoterminal slave
int      pty_utils_master_open           (char *slave_name,
                                          size_t slave_name_len);
// Create a child process that is connected to the parent by a pseudoterminal pair
pid_t    pty_utils_fork                  (int *master_fd,
                                          char *slave_name,
                                          size_t sn_len,
                                          const struct termios *slave_termios,
                                          const struct winsize *slave_ws);
// Place the terminal in raw mode, so that all input characters are
// passed directly to the program without being modified by the terminal driver
int      pty_utils_properties_set_raw    (int fd,
                                          struct termios *prevTermios);

#endif //   __PTYUTILS_H
