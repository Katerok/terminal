#include <gtk/gtk.h>

#include <stdlib.h>
#include <string.h>

#include "ControlCharacters.h"
#include "TerminalTextView.h"

struct _TerminalTextView
{
  GtkTextView parent;
};

typedef struct _TerminalTextViewPrivate TerminalTextViewPrivate;

struct _TerminalTextViewPrivate
{
   char     *string_tail;
   ssize_t   string_tail_length;
};

G_DEFINE_TYPE_WITH_PRIVATE (TerminalTextView, terminal_text_view, GTK_TYPE_TEXT_VIEW);

static void
terminal_text_view_init (TerminalTextView *text_view)
{
  TerminalTextViewPrivate *priv;
  priv = terminal_text_view_get_instance_private (text_view);

  priv->string_tail = NULL;
  priv->string_tail_length = 0;
}

static void
terminal_text_view_class_init (TerminalTextViewClass *class)
{
}

static ssize_t
terminal_text_view_process_control_sequence (TerminalTextView *text_view,
                                             const char *string,
                                             ssize_t length)
{
  ssize_t i = 2;
  if (length < 2)
    return -1;


 while (i < length)
  {
    switch (string[i])
      {
      case '@':
      case 'A':
      case 'B':
      case 'C':
      case 'D':
      case 'E':
      case 'F':
      case 'G':
      case 'H':
      case 'J':
      case 'K':
      case 'L':
      case 'M':
      case 'P':
      case 'X':
      case 'a':
      case 'c':
      case 'd':
      case 'e':
      case 'f':
      case 'g':
      case 'h':
      case 'l':
      case 'm':
      case 'n':
      case 'q':
      case 'r':
      case 's':
      case 'u':
      case '`':
        printf ("\e[34mSKIPPED SEQUENCE\e[0m: "
                "\e[96mESC\e[0m%*.*s\n",
                (int)i, (int)i, string + 1);
        return i;
      }
    ++i;
  }

  return -1;
}

static ssize_t
terminal_text_view_process_character_set_sequence (TerminalTextView *text_view,
                                                   const char *string,
                                                   ssize_t length)
{
  return 0;
}

static ssize_t
terminal_text_view_process_operating_system_command (TerminalTextView *text_view,
                                                     const char *string,
                                                     ssize_t length)
{
  ssize_t i = 2;
  if (length < 2)
    return -1;

  while (i < length)
  {
    if (string[i] == BEL)
      {
        printf ("\e[34mSKIPPED SEQUENCE\e[0m: "
                "\e[96mESC\e[0m%*.*s\e[96mBEL\e[0m\n",
                (int)i, (int)i, string + 1);
        return i;
      }
    ++i;
  }

  return -1;
}

static ssize_t
terminal_text_view_process_screen_alignment_test (TerminalTextView *text_view,
                                                  const char *string,
                                                  ssize_t length)
{
  return 0;
}

static ssize_t
terminal_text_view_process_escape_sequence (TerminalTextView *text_view,
                                            const char *string,
                                            ssize_t length)
{
  if (length < 2)
    return -1;

  switch (string[1])
    {
    case 'c':
    case 'D':
    case 'E':
    case 'H':
    case 'M':
    case 'Z':
    case '7':
    case '8':
    case '>':
    case '=':
      printf ("\e[31m'ESC %c' is not supported\e[0m\n", string[1]);
      return 1;
    case '[':
      return terminal_text_view_process_control_sequence (text_view,
                                                          string,
                                                          length);
    case '%':
    case '(':
    case ')':
      return terminal_text_view_process_character_set_sequence (text_view,
                                                                string,
                                                                length);
    case '#':
      return terminal_text_view_process_screen_alignment_test (text_view,
                                                               string,
                                                               length);
    case ']':
      return terminal_text_view_process_operating_system_command (text_view,
                                                                  string,
                                                                  length);
    default:
      printf ("\e[31mUnrecognized escape sequence: ESC %c\e[0m\n", string[1]);
      break;
    }

  return -1;
}

static void
terminal_text_view_set_string_tail (TerminalTextView *text_view,
                                    const char *string,
                                    ssize_t length)
{
  TerminalTextViewPrivate *priv;
  priv = terminal_text_view_get_instance_private (text_view);

  if (priv->string_tail != NULL)
    free (priv->string_tail);

  priv->string_tail_length = length;
  priv->string_tail = strndup (string, length);
}

static void
terminal_text_view_process_string (TerminalTextView *text_view,
                                   const gchar *string,
                                   ssize_t length)
{
  GtkTextBuffer           *buffer;
  ssize_t                  i = 0;
  ssize_t                  substring_length;

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (text_view));

  while (i < length)
    {
      switch (string[i])
        {
        case ESC:
          substring_length =
            terminal_text_view_process_escape_sequence (text_view,
                                                        string + i, length - i);

          if (substring_length == -1)
            {
              /* We get here if there is an uncompleted control sequence
               * in the tail of the string.
               * Call terminal_text_view_set_string_tail() to save
               * remaining string tail for further processing */
              terminal_text_view_set_string_tail (text_view,
                                                  string + i, length - i);
              return;
            }

          i = i + substring_length;
          break;
        default:
          gtk_text_buffer_insert_at_cursor (buffer, string + i, 1);
          break;
        }
      ++i;
    }
}

void
terminal_text_view_insert (TerminalTextView *text_view,
                           const char *string,
                           ssize_t length)
{
  TerminalTextViewPrivate *priv;
  gchar                   *string_to_view = NULL;
  gsize                    string_to_view_length;

  priv = terminal_text_view_get_instance_private (text_view);

  if (priv->string_tail != NULL)
    {
      /* Consatinate string that remains from previous processing
       * and currently passed string */
      string_to_view_length = priv->string_tail_length + length;
      string_to_view = malloc (string_to_view_length * sizeof (char));

      strncpy (string_to_view, priv->string_tail, priv->string_tail_length);
      strncpy (string_to_view + priv->string_tail_length, string, length);

      /* Reset string that remains from previous processing */
      terminal_text_view_set_string_tail (text_view, NULL, 0);

      /* Pass concatinated string for processing */
      terminal_text_view_process_string (text_view, string_to_view,
                                         string_to_view_length);

      free (string_to_view);
    }
  else
    {
      /* Pass string for processing */
      terminal_text_view_process_string (text_view, string, length);
    }
}

TerminalTextView *
terminal_text_view_new (void)
{
  TerminalTextView *new_object = g_object_new (TERMINAL_TEXT_VIEW_TYPE, NULL);
  gtk_text_view_set_editable (GTK_TEXT_VIEW (new_object), FALSE);
  return new_object;
}

